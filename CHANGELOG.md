# Change Log

All notable changes to this project will be documented in this file.


2.4 (06-May-2018)

Features:

  * Comment field updated in the quickcal.desktop file
  * quickcal.appdata.xml file introduced for software manager description
  * quickcal.appdata.xml includes screenshots for the application
  * setup.py updated for version numbers and included above file
  * quickcal.css introduced for theming
  * Files appdata and desktop are renamed to specification


2.3 (06-May-2018)

Features:

  * num script path updated in the quickcal script


2.2 (25-Apr-2018)

Features:

  * Changes to setup.py file to use setuptools for the setup


2.1 (26-Dec-2017)

Features:

  * Completion of Porting to Python 3.5 ensuring backward compatibility with 2.7


2.0 (16-Dec-2017)

Features:

  * Porting to Python 3.5 ensuring backward compatibility with 2.7


1.0 (22-Nov-2017)

Features:

  * Initial release. 


